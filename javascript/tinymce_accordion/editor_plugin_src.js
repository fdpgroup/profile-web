(function() {
    tinymce.PluginManager.requireLangPack('accordion');

    tinymce.create('tinymce.plugins.AccordionPlugin', {

        init : function(ed, url) {

            ed.addCommand('mceAccordion', function() {
                var content = '<p>Body Content</p>';
                if (ed.selection.getContent().length > 0) {
                    content = ed.selection.getContent();
                }
                ed.selection.setContent('<div class="accordion"><div class="accordion-title">Title</div><div class="accordion-body"><div class="accordion-content">' + content + '</div></div><div class="accordion-clearer"></div></div>');
            });

            ed.addButton('accordion', {
                title : 'accordion.desc',
                cmd : 'mceAccordion',
                image : url + '/img/accordion.png'
            });
        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : 'Accordion Plugin',
                author : 'Luke Skelding',
                authorurl : 'https://www.fdpgroup.co.uk',
                version : "1.0"
            };
        }
    });

    tinymce.PluginManager.add('accordion', tinymce.plugins.AccordionPlugin);
})();
