<?php


class ThemedEmail extends StyledHtmlEmail {
    
	private static $template = 'ThemedEmail';
	
    public function populateTemplate($data) {
        $this->setTemplate($this->stat('template'));
        $body = new SSViewer(get_class($this));
        parent::populateTemplate(new ArrayData(array(
            'Body' => $body->process($data)
        )));
    }
}