<?php


class ExportableBulkLoader extends CsvBulkLoader {
    
	private static $has_one_classes = array();
	private static $has_many_classes = array();
	private static $many_many_classes = array();
	
    public function getImportSpec() {
        $spec = parent::getImportSpec();
		$export = Config::inst()->get($this->objectClass, 'export_fields', Config::FIRST_SET);
		foreach ($export as $field => $label) {
			if (array_key_exists($field, $spec['fields'])) {
				$spec['fields'][$field] = $label;
			}
		}
		return $spec;
    }
	
	public static function setHasOneRelation(&$obj, $value, $record) {
		$classes = Config::inst()->get(get_called_class(), 'has_one_classes', Config::FIRST_SET);
		$column = array_search($value, $record);
		if (array_key_exists($column, $classes)) {
			$relation_cls = $classes[$column]['class_name'];
			$foreign_key = $classes[$column]['foreign_key'];
			$value_attr = $classes[$column]['value_attr'];
			$create_new = array_key_exists('create_new', $classes[$column]) ? $classes[$column]['create_new'] : true;
			if (is_null($value) || empty($value)) {
				$obj->$foreign_key = 0;
			}
			else {
				$relation = singleton($relation_cls)->get()->filter(array($value_attr => $value))->First();
				if (is_null($relation)) {
					if ($create_new) {
						$relation = new $relation_cls();
						$relation->$value_attr = $value;
						$relation->write();
						$obj->$foreign_key = $relation->ID;
					}
					else {
						$obj->$foreign_key = 0;
					}
				}
				else {
					$obj->$foreign_key = $relation->ID;
				}
			}
		}
	}
	
	public static function setHasManyRelations(&$obj, $value, $record) {
		$classes = Config::inst()->get(get_called_class(), 'has_many_classes', Config::FIRST_SET);
		$column = array_search($value, $record);
		if (array_key_exists($column, $classes)) {
			$relation_cls = $classes[$column]['class_name'];
			$foreign_key = $classes[$column]['foreign_key'];
			$has_many = $classes[$column]['has_many'];
			$value_attr = $classes[$column]['value_attr'];
			if (is_null($value) || empty($value)) {
				$obj->$has_many()->removeAll();
			}
			else {
				$ids = array();
				foreach (explode(',', $value) as $name) {
					$name = trim($name);
					if (!empty($name)) {
    					$relation = $relation_cls::get()->filter(array(
    						$foreign_key => $obj->ID,
    						$value_attr => $name
    					))->First();
    					if (is_null($relation)) {
    						$relation = new $relation_cls();
    						$relation->$value_attr = $name;
    						$relation->$foreign_key = $obj->ID;
    						$relation->write();
    					}
    					$ids[] = $relation->ID;
    				}
				}
				if ($obj->exists()) {
					$obj->$has_many()->removeMany($obj->$has_many()->exclude(array('ID' => $ids))->column('ID'));
				}
			}
		}
	}
	
	public static function setManyManyRelations(&$obj, $value, $record) {
		$classes = Config::inst()->get(get_called_class(), 'many_many_classes', Config::FIRST_SET);
		$column = array_search($value, $record);
		if (array_key_exists($column, $classes)) {
			$relation_cls = $classes[$column]['class_name'];
			$many_many = $classes[$column]['many_many'];
			$value_attr = $classes[$column]['value_attr'];
			if (is_null($value) || empty($value)) {
				$obj->$many_many()->removeAll();
			}
			else {
				$ids = array();
				foreach (explode(',', $value) as $name) {
    				$name = trim($name);
    				if (!empty($name)) {
    					$relation = $relation_cls::get()->filter(array($value_attr => $name))->First();
    					if (is_null($relation)) {
    						$relation = new $relation_cls();
    						$relation->$value_attr = $name;
    						$relation->write();
    					}
    					if (!$obj->exists() || $obj->$many_many()->filter(array('ID' => $relation->ID))->count() < 1) {
    						$obj->$many_many()->add($relation);
    					}
    					$ids[] = $relation->ID;
    				}
				}
				if ($obj->exists()) {
					$obj->$many_many()->removeMany($obj->$many_many()->exclude(array('ID' => $ids))->column('ID'));
				}
			}
		}
	}
	
	protected function processRecord($record, $columnMap, &$results, $preview = false) {
    	try {
        	parent::processRecord($record, $columnMap, $results, $preview);
    	}
    	catch (ValidationException $e) {
        	$results->addSkipped($record);
    	}
    }
}