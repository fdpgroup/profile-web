<?php


class GoogleTagManager {
	
	private static $_data_layer_init = array();
	private static $_data_layer_pushes = array();
	private static $_data_layer_queue = array();
	
	public static function init($data) {
		self::$_data_layer_init[] = $data;
	}
	
	public static function push($data) {
		self::$_data_layer_pushes[] = $data;
	}
	
	public static function clear_pushes() {
		self::$_data_layer_pushes = array();
	}
	
	public static function queue($key, $data) {
		self::$_data_layer_queue[$key] = $data;
	}
	
	public static function clear_queue() {
		self::$_data_layer_queue = array();
	}
	
	public static function unqueue($key) {
		unset(self::$_data_layer_queue[$key]);
	}
	
	public static function data_layer_init() {
		return self::$_data_layer_init;
	}
	
	public static function data_layer_pushes() {
		return self::$_data_layer_pushes;
	}
	
	public static function data_layer_queue() {
		return self::$_data_layer_queue;
	}
}