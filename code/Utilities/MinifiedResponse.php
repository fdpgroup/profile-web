<?php

use zz\Html\HTMLMinify;

class MinifiedResponse extends SS_HTTPResponse {
	
	public function setBody($body) {
		$this->body = $body ? (string)$body : $body;
		if (!Director::isDev() && preg_match('/text\/html/', $this->getHeader('Content-Type'))) {
			$this->body = HTMLMinify::minify(
				$this->body, array('optimizationLevel' => HTMLMinify::OPTIMIZATION_ADVANCED)
			);
		}
	}
}