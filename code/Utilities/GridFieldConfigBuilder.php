<?php


class GridFieldConfigBuilder {

    public static function build($components) {
        $config = GridFieldConfig::create();
        foreach ($components as $cls => $settings) {
            $add = true;
            if (is_numeric($cls)) {
                $cls = $settings;
                $settings = null;
            }
            if (preg_match('/^(.*?)\[[a-zA-Z0-9]*\]$/', $cls, $match)) {
                $cls = $match[1];
            }
            if (!empty($settings)) {
                if (array_key_exists('Permission', $settings)) {
                    $add = Permission::check($settings['Permission']);
                }
                if (array_key_exists('Constructor', $settings)) {
                    $component = new $cls(...$settings['Constructor']);
                }
                else {
                    $component = new $cls();
                }
                if (array_key_exists('Options', $settings)) {
                    foreach ($settings['Options'] as $setter => $value) {
                        $method = "set{$setter}";
                        if (method_exists($component, $setter)) {
                            $component->$setter(...$value);
                        }
                        else if (method_exists($component, $method)) {
                            $component->$method(...$value);
                        }
                        else if (property_exists($component, $setter)) {
                            $component->$setter = $value;
                        }
                    }
                }
            }
            else {
                $component = new $cls();
            }
            if ($add) {
                $config->addComponent($component);
            }
        }
        return $config;
    }
}
