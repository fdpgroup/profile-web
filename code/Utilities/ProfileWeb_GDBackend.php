<?php


class ProfileWeb_GDBackend extends GDBackend {
	
	private static $default_png_compression = 6;
	private static $tinyfy_api_key = '';
	
	public function __construct($filename = null, $args = array()) {
		parent::__construct($filename, $args);
		$this->png_compression = $this->config()->default_png_compression;
	}
	
	protected $png_compression;
	
	public function setPNGCompression($compression) {
		$this->png_compression = $compression;
	}
	
	protected $tinyfy = false;
	
	public function setTinyfy($tinyfy) {
		$this->tinyfy = $tinyfy;
	}
	
	public function writeTo($filename) {
		$this->makeDir(dirname($filename));
		
		if($filename) {
			if(file_exists($filename)) list($width, $height, $type, $attr) = getimagesize($filename);
			
			if(file_exists($filename)) unlink($filename);

			$ext = strtolower(substr($filename, strrpos($filename,'.')+1));
			if(!isset($type)) switch($ext) {
				case "gif": $type = IMAGETYPE_GIF; break;
				case "jpeg": case "jpg": case "jpe": $type = IMAGETYPE_JPEG; break;
				default: $type = IMAGETYPE_PNG; break;
			}

			// if $this->interlace != 0, the output image will be interlaced
			imageinterlace ($this->gd, $this->interlace);
			
			// if the extension does not exist, the file will not be created!
			
			switch($type) {
				case IMAGETYPE_GIF: imagegif($this->gd, $filename); break;
				case IMAGETYPE_JPEG: imagejpeg($this->gd, $filename, $this->quality); break;
				
				// case 3, and everything else
				default: 
					// Save them as 8-bit images
					// imagetruecolortopalette($this->gd, false, 256);
					imagepng($this->gd, $filename, $this->png_compression); break;
			}
			if (file_exists($filename)) {
				@chmod($filename,0664);
				
				if (($type == IMAGETYPE_PNG || $type == IMAGETYPE_JPEG) && $this->tinyfy) {
					$key = $this->config()->tinyfy_api_key;
					if (!empty($key)) {
						\Tinify\setKey($key);
						$source = \Tinify\fromFile($filename);
						$source->toFile($filename);
					}
				}
			}
		}
	}
}