<?php


class ProfileWeb_BulkLoader_Result extends Extension {
    
    protected $skipped = 0;
    
    public function SkippedCount() {
		return $this->skipped;
	}
	
	public function Skipped() {
		return $this->skipped;
	}
    
    public function addSkipped($obj, $message = null) {
		$this->skipped++;
		return 0;
	}
}