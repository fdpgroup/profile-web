<?php


class ProfileWeb_ModelAdmin extends Extension {
    
    public function getExportFields() {
        return singleton($this->owner->modelClass)->exportFields();
    }
}