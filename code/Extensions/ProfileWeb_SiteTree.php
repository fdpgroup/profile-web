<?php


class ProfileWeb_SiteTree extends DataExtension {
	
	public function MetaTags(&$tags) {
		if (!empty($this->owner->MetaKeywords)) {
            $tags .= "<meta name=\"keywords\" content=\"" . Convert::raw2att($this->owner->MetaKeywords) . "\" />\n";
        }
        if (!empty($this->owner->MetaRobots)) {
            $tags .= "<meta name=\"robots\" content=\"" . Convert::raw2att($this->owner->MetaRobots) . "\" />\n";
        }
	}
}


class ProfileWeb_ContentController extends Extension {
	
	private static $meta_tagged_objects = array();
	
	public function afterCallActionHandler($request, $action) {
		foreach (Config::inst()->get($this->owner->class, 'meta_tagged_objects') as $obj) {
			if ($this->owner->hasMethod($obj)) {
				$this->owner->populateMeta($this->owner->$obj());
			}
		}
	}
	
	public function populateMeta($tagged) {
		if (!empty($tagged) && $tagged->hasExtension('ProfileWeb_MetaTagged')) {
			$record = $this->owner->data();
			if (!empty($tagged->MetaTitle)) {
				$record->MetaTitle = $tagged->MetaTitle;
			}
			if (!empty($tagged->MetaDescription)) {
				$record->MetaDescription = $tagged->MetaDescription;
			}
			if (!empty($tagged->MetaKeywords)) {
				$record->MetaKeywords = $tagged->MetaKeywords;
			}
			if (!empty($tagged->MetaRobots)) {
				$record->MetaRobots = $tagged->MetaRobots;
			}
			if (!empty($tagged->ExtraMeta)) {
				$record->ExtraMeta = $tagged->ExtraMeta;
			}
		}
	}
	
	public function IsDev() {
        return Director::isDev();
    }
    
    public function IsTest() {
        return Director::isTest();
    }
    
    public function IsLive() {
        return Director::isLive();
    }
}