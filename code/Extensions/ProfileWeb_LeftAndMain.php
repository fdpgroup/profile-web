<?php


class ProfileWeb_LeftAndMain extends LeftAndMainExtension {

    private static $enable_tinymce_accordion = false;
    private static $include_tinymce_accordion_css = false;

    public function init() {
        if (count(Config::inst()->get('CMSAttributionController', 'attributions')) < 1) {
            CMSMenu::remove_menu_item('CMSAttributionController');
        }

        if (Config::inst()->get(get_class($this->owner), 'enable_tinymce_accordion')) {
            $config = HtmlEditorConfig::get('cms');
            if (Config::inst()->get(get_class($this->owner), 'include_tinymce_accordion_css')) {
                ProfileWeb_LeftAndMain::append_html_editor_setting(
                    $config, 'content_css', 'profileweb/css/tinymce_accordion.css'
                );
            }
            ProfileWeb_LeftAndMain::append_html_editor_setting(
                $config, 'custom_elements', 'div[.accordion|.accordion-title|.accordion-body|.accordion-content|.accordion-clearer]'
            );
            ProfileWeb_LeftAndMain::append_html_editor_setting(
                $config, 'valid_children', 'div.accordion[div.accordion-title,div.accordion-body,div.accordion-clearer],div.accordion-body[div.accordion-content]'
            );
            ProfileWeb_LeftAndMain::append_html_editor_setting(
                $config, 'extended_valid_elements', 'div[.accordion|.accordion-title|.accordion-body|.accordion-content|.accordion-clearer]'
            );
            $config->enablePlugins(array('accordion' => str_replace($_SERVER['DOCUMENT_ROOT'], '', BASE_PATH) . '/profileweb/javascript/tinymce_accordion/editor_plugin_src.js'));
            $config->addButtonsToLine(1, '|,accordion');
        }
    }

    public static function append_html_editor_setting(&$config, $setting, $addition) {
        $original = $config->getOption($setting);
        $config->setOption($setting, empty($original) ? $addition : implode(',', array($original, $addition)));
    }
}
