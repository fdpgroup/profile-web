<?php


class ProfileWeb_DataList extends Extension {
    
    public function groupBy($groupby) {
        return $this->owner->alterDataQuery(function($query) use ($groupby){
            $query->groupby($groupby);
        });
    }
}