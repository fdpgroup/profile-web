<?php

class ProfileWeb_Slugged extends DataExtension {

    private static $slugged_fields = array();
	private static $db = array(
		'Slug' => 'Varchar(100)'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->removeFieldFromTab('Root.Main', 'Slug');
	}

	public function onBeforeWrite() {
		$slugged = $this->owner->stat('slugged_fields');
		if (!empty($slugged) && !is_null($slugged)) {
    		$slug = array();
            foreach ($slugged as $field) {
                $getter = "get{$field}";
                if ($this->owner->hasMethod($getter)) {
                    $slug[] = $this->owner->$getter();
                }
                else if ($this->owner->hasMethod($field)) {
                    $slug[] = $this->owner->$field();
                }
                else {
                    $slug[] = $this->owner->$field;
                }
            }
            $slug = trim(implode(' ', $slug));
            $this->owner->Slug = StringHelpers::form_slug($slug);
		}
	}
}
