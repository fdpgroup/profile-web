<?php


class ProfileWeb_Debug extends Extension {
    
    public static function output($data) {
        if (is_string($data)) {
            echo $data;
        }
        else {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
        }
        exit();
    }
}