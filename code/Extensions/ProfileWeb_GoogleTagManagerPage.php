<?php


class ProfileWeb_GoogleTagManagerPage extends DataExtension {
	
	private static $db = array(
		'TagManagerEvent' => 'Varchar(100)'
	);
	
	public function updateSettingsFields(FieldList $fields) {
		$fields->addFieldToTab(
			'Root.GoogleTagManager',
			TextField::create('TagManagerEvent', 'Event')
		);
	}
}


class ProfileWeb_GoogleTagManagerPage_Controller extends Extension {
	
	public function onAfterInit() {
		if (!empty($this->owner->TagManagerEvent)) {
			GoogleTagManager::push(array('event' => $this->owner->TagManagerEvent));
		}
	}
	
	public function GoogleTagManagerScript() {
		$id = SiteConfig::current_site_config()->GoogleTagManagerID;
		if (!empty($id)) {
			$pushes = ArrayList::create();
			foreach (GoogleTagManager::data_layer_pushes() as $push) {
				$pushes->push(DBField::create_field('HTMLText', json_encode($push)));
			}
			return $this->owner->renderWith(array('GoogleTagManagerScript'), array(
				'ID' => $id,
				'DataLayer' => ArrayData::create(array(
					'Init' => json_encode(GoogleTagManager::data_layer_init()),
					'Queue' => json_encode(GoogleTagManager::data_layer_queue()),
					'Pushes' => $pushes
				))
			));
		}
		else {
			return '';
		}
	}
	
	public function GoogleTagManagerNoScript() {
		$id = SiteConfig::current_site_config()->GoogleTagManagerID;
		if (!empty($id)) {
			return $this->owner->renderWith(
				array('GoogleTagManagerNoScript'),
				array('ID' => $id)
			);
		}
		else {
			return '';
		}
	}
}