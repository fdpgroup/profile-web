<?php


class ProfileWeb_GoogleTagManagerConfig extends DataExtension {
	
	private static $db = array(
		'GoogleTagManagerID' => 'Varchar(100)'
	);
	
	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab(
			$this->owner->hasExtension('GoogleConfig') ? 'Root.GoogleAnalytics' : 'Root.GoogleTagManager',
			TextField::create('GoogleTagManagerID', 'Tag Manager ID')
		);
	}
}