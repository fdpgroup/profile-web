<?php


class ProfileWeb_MetaTagged extends DataExtension {
    
    private static $db = array(
        'MetaTitle' => 'Varchar(200)',
        'MetaDescription' => 'Text',
        'MetaKeywords' => 'Text',
        'MetaRobots' => 'Varchar(200)',
        'ExtraMeta' => 'Text'
    );
    
    public function updateCMSFields(FieldList $fields) {
        $fields->removeByName('MetaTitle');
        $fields->removeByName('MetaDescription');
        $fields->removeByName('MetaKeywords');
        $fields->removeByName('MetaRobots');
        $fields->removeByName('ExtraMeta');
		$fields->removeByName('Metadata');
        $fields->addFieldToTab(
            'Root.Main',
            ToggleCompositeField::create('Metadata', 'Metadata', FieldList::create(
                TextField::create('MetaTitle', 'Title'),
                TextareaField::create('MetaDescription', 'Description')->setRows(3),
                TextareaField::create('MetaKeywords', 'Keywords')->setRows(3),
                TextField::create('MetaRobots', 'Robots'),
                TextareaField::create('ExtraMeta', 'Custom Meta Tags')->setRows(3)
            ))->setHeadingLevel(4)
        );
    }
}