<?php


class ProfileWeb_Image extends DataExtension {

	public function AlignedCrop($width, $height, $horizontal = 'Centre', $vertical = 'Centre') {
		return $this->owner->getFormattedImage('AlignedCrop', $width, $height, $horizontal, $vertical);
	}

	public function generateAlignedCrop(Image_Backend $backend, $width, $height, $horizontal = 'Centre', $vertical = 'Centre') {
		$src_width = $backend->getWidth();
		$src_height = $backend->getHeight();
		if ($width > $src_width || $height > $src_height) {
			return $backend;
		}
		else {
			$ratio = $src_width / $src_height;
			if ($ratio < 1) {
				$src_width = $width;
				$src_height = $height * $ratio;
			}
			else {
				$src_width = $width * $ratio;
				$src_height = $height;
			}
			$backend = $backend->resize($src_width, $src_height);
			$top = $left = 0;
			switch ($horizontal) {
				case 'Centre':
					$left = ($src_width - $width) / 2;
					break;
				case 'Left':
					$left = 0;
					break;
				case 'Right':
					$left = $src_width - $width;
					break;
			}
			switch ($vertical) {
				case 'Centre':
					$top = ($src_height - $height) / 2;
					break;
				case 'Top':
					$top = 0;
					break;
				case 'Bottom':
					$top = $src_height - $height;
					break;
			}
			return $backend->crop($top, $left, $width, $height);
		}
	}

    public function Colourised($r, $g, $b, $a) {
        return $this->owner->getFormattedImage('Colourised', $r, $g, $b, $a);
    }

    public function generateColourised(Image_Backend $backend, $r, $g, $b, $a) {
        $width = $backend->getWidth();
        $height = $backend->getHeight();
        $canvas = imagecreatetruecolor($width, $height);
        $original = $backend->getImageResource();
        imagecopy($canvas, $original, 0, 0, 0, 0, $width, $height);
        $colour = imagecolorallocatealpha($canvas, $r, $g, $b, 127.0 * (1 - $a));
        imagefilledrectangle($canvas, 0, 0, $width, $height, $colour);
        $output = clone $backend;
        $output->setImageResource($canvas);
        return $output;
	}
}
