<?php


class ProfileWeb_MinifiedContentController extends Extension {
	
	public function onBeforeInit() {
		if (is_a($this->owner, 'ContentController')) {
			$this->owner->response = new MinifiedResponse();
		}
	}
}