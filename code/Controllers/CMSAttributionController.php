<?php


class CMSAttributionController extends LeftAndMain {

    private static $menu_title = 'Attributions';
    private static $url_segment = 'attributions';
	private static $url_rule = '/$Action/$ID/$OtherID';
    private static $menu_icon = 'profileweb/images/CMSAttributionController.png';
    private static $menu_priority = -100;

    private static $attributions = array();

    public function Index($request) {
        $attributions = ArrayList::create();
        foreach (Config::inst()->get('CMSAttributionController', 'attributions') as $attribution) {
            $attributions->push(ArrayData::create(array(
                'Title' => $attribution['title'],
                'URL' => $attribution['url'],
                'Author' => $attribution['author'],
                'Licence' => ArrayData::create(array(
                    'Title' => $attribution['licence'],
                    'URL' => $attribution['licence_url']
                )),
                'Use' => $attribution['use']
            )));
        }
        return $this->_response($request, array('Attributions' => $attributions));
    }

    private function _response($request, $fields = array()) {
		if ($pjax = $request->getHeader('X-Pjax')) {
			return $this->renderWith(sprintf('CMSAttributionController_%s', $pjax), $fields);
		}
		else {
			return $fields;
		}
	}
}
