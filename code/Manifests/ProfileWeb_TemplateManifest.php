<?php

/* This replaces SS_TemplateManifest in framework/core/Core.php on line 121 */    

class ProfileWeb_TemplateManifest extends SS_TemplateManifest {
    
    private static $_types = array(
        'Layout', 'Includes', 'email', 'forms'
    );
    
    public function handleFile($basename, $pathname, $depth) {
		$layout_needle = 'Layout/';
		$layout_pos = strpos($pathname, $layout_needle);
		if ($layout_pos !== false) {
    		$layout = substr($pathname, $layout_pos + strlen($layout_needle));
    		if (substr_count($layout, '/') > 0) {
        		$parts = explode('/', str_replace('.ss', '', $layout));
        		$last = count($parts) - 1;
        		if ($parts[$last] == 'Index') {
            		unset($parts[$last]);
        		}
        		$basename = sprintf('%s.ss', implode('_', $parts));
    		}
		}
		
		$projectFile = false;
		$theme = null;

		if (strpos($pathname, $this->base . '/' . THEMES_DIR) === 0) {
			$start = strlen($this->base . '/' . THEMES_DIR) + 1;
			$theme = substr($pathname, $start);
			$theme = substr($theme, 0, strpos($theme, '/'));
			$theme = strtok($theme, '_');
		} 
		else if($this->project && (strpos($pathname, $this->base . '/' . $this->project .'/') === 0)) { 
			$projectFile = true;
		}
		
		$type = 'main';
		foreach (self::$_types as $t) {
    		if (strrpos(strtolower($pathname), strtolower(sprintf('/%s/', $t))) !== false) {
        		$type = $t;
        		break;
    		}
		}

        $name = strtolower(substr($basename, 0, -3));

		if ($theme) {
			$this->templates[$name]['themes'][$theme][$type] = $pathname;
		} else if($projectFile) {
			$this->templates[$name][$this->project][$type] = $pathname;
		} else {
			$this->templates[$name][$type] = $pathname;
		}
	}
}